def getname():
    name = input ("Please enter your name.")
    if name == 'Mr Dunn':
        print ("Hello Sir.")
        name = 'Sir'
    else:
        print ("Hello", name)
    return name


def main():
    name = getname()

    homework = input("Have you done your homework?")
    if homework.lower() == 'yes':
        print ("Well done!")
    elif homework.lower() == 'no':
        print ("Detention.")
    else:
        answer = input ("Please answer Yes or No.")
        if answer.lower() == 'yes':
            print ("Well done!")
        elif answer.lower() == 'no':
            print ("Detention.")
        else:
            again = input ("Please answer Yes or No.")
            if again.lower() == 'yes':
                print ("Well done!")
            elif again.lower() == 'no':
                print ("Detention.")
            else:
                final = input ("Last chance. Please answer Yes or No.")
                if final.lower() == 'yes':
                    print ("Good.")
                    print ("Now was that really that hard?")
                elif final.lower == 'no':
                    print ("Detention.")
                else:
                    print ("Fine. Detention then.")
                    print ("Bye", name)
                    print ("Have fun!")


if __name__ == '__main__':
    main()
